new Vue({
  el: "#app",
  data: {
    colors: [
      {
        classButton: 'btn-primary',
        classButtonOutline: 'btn-outline-primary',
        colorHex: '#00a2c2',
        label: 'primary',
        variableName: '$color-blue',
      },
      {
        classButton: 'btn-secondary',
        classButtonOutline: 'btn-outline-secondary',
        colorHex: '#6c7788',
        label: 'secondary',
        variableName: '$color-gray-medium',
      },
      {
        classButton: 'btn-success',
        classButtonOutline: 'btn-outline-success',
        colorHex: '#3cb185',
        label: 'success',
        variableName: '$color-green',
      },
      {
        classButton: 'btn-info',
        classButtonOutline: 'btn-outline-info',
        colorHex: '#9d72af',
        label: 'info',
        variableName: '$color-purple',
      },
      {
        classButton: 'btn-warning',
        classButtonOutline: 'btn-outline-warning',
        colorHex: '#e89d51',
        label: 'warning',
        variableName: '$color-orange',
      },
      {
        classButton: 'btn-danger',
        classButtonOutline: 'btn-outline-danger',
        colorHex: '#f0545e',
        label: 'danger',
        variableName: '$color-red',
      },
      {
        classButton: 'btn-light',
        classButtonOutline: 'btn-outline-light',
        colorHex: '#9fb0c8',
        label: 'light',
        variableName: '$color-gray-light',
      },
      {
        classButton: 'btn-dark',
        classButtonOutline: 'btn-outline-dark',
        colorHex: '#393f49',
        label: 'dark',
        variableName: '$color-gray',
      }
    ]
  },
  methods: {
    applyColorHex: function(event){
      return 'color: ' + event
    }
  },
})